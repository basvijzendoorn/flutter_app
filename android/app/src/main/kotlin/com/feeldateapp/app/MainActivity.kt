package com.feeldateapp.app

import android.os.Bundle

import io.flutter.plugins.GeneratedPluginRegistrant
import io.flutter.app.FlutterActivity

class MainActivity: FlutterActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    GeneratedPluginRegistrant.registerWith(this)
  }
}
