import 'dart:convert';

import 'package:date_app/person.dart';
import 'package:test/test.dart';
import 'package:mockito/mockito.dart';
import "package:http/http.dart" as http;

class MockClient extends Mock implements http.Client {}

void main() {

  test("Retrieve a person", () async {
    var id = 1;
    var httpBody = "{\"id\":1,\"first_name\":\"balie\",\"middle_name\":\"\",\"last_name\":\"\",\"age\":12,\"image\":\"http://192.168.43.165:8000/media/golf.png\"}";
    var statusCode = 200;

    MockClient mockClient = new MockClient();

    when(mockClient.get(Uri.parse(PersonFutures.baseUrl + '/persons/' + id.toString() + '/')))
        .thenAnswer((_) async => http.Response(httpBody, statusCode));

    PersonFutures personFutures = new PersonFutures(mockClient);

    Person person = await personFutures.retrievePerson(id);
    expect(person.first_name, "balie");

  });

  test("Retrieve persons", () async {
    var statusCode = 200;
    var httpBody = "[{\"id\": 1,\"first_name\": \"barend\",\"middle_name\": \"\",\"last_name\": \"\",\"age\": 12,\"image\": \"http://192.168.1.11:8000/media/golf.png\"},{\"id\": 2,\"first_name\": \"doris\",\"middle_name\": \"\",\"last_name\": \"\",\"age\": 15,\"image\": \"http://192.168.1.11:8000/media/golf_q9SEzCz.png\"}]";

    MockClient mockClient = new MockClient();

    when(mockClient.get(Uri.parse(PersonFutures.baseUrl + '/persons/')))
      .thenAnswer((_) async => http.Response(httpBody, statusCode));

    PersonFutures personFutures = new PersonFutures(mockClient);

    List<Person> persons = await personFutures.retrievePersons();
    expect(persons[0].first_name, "barend");
    expect(persons[1].first_name, "doris");
  });

  test("Retrieve likes", () async {
    var id = 1;
    var statusCode = 200;
    var httpBody = "{\"likes\": [{\"like_id\": 2,\"person_id\": 2,\"datetime\": \"2020-04-03 12:45:00\"}],\"liked_by\": [],\"matches\": []}";

    MockClient mockClient = new MockClient();
    when(mockClient.get(Uri.parse(PersonFutures.baseUrl + '/persons/' + id.toString() + '/likes/')))
      .thenAnswer((_) async => http.Response(httpBody, statusCode));

    PersonFutures personFutures = new PersonFutures(mockClient);

    Likes likes = await personFutures.retrieveLikes(id);
    expect(likes.likes.first.like_id, 2);
    expect(likes.likes.first.person_id, 2);
  });

  test("Create a like", () {
    var likeJson = "{\"like_id\": 1,\"person_id\": 2,\"datetime\": \"2020-04-03 12:45:00\"}";

    var like = Like.fromJson(jsonDecode(likeJson));

    expect(like.like_id, 1);
    expect(like.person_id, 2);
    expect(like.dateTime, DateTime.parse("2020-04-03 12:45:00"));
  });

  test("Create likes", () {
    var likesJson = "{\"likes\": [{\"like_id\": 2,\"person_id\": 2,\"datetime\": \"2020-04-03 12:45:00\"}],\"liked_by\": [],\"matches\": []}";

    var likes = Likes.fromJson(jsonDecode(likesJson));

    expect(likes.likes.length, 1);
    expect(likes.liked_by.length,0);
    expect(likes.matches.length,0);
  });

}
