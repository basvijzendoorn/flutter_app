import 'dart:convert';
import 'dart:io';

import 'package:age/age.dart';
import 'package:date_app/user.dart';
import "package:http/http.dart" as http;


class Person {
  int id;
  bool show;
  String first_name;
  String last_name;
  String gender;
  DateTime date_of_birth;
  int age;
  String image_url;
  String occupation;

  Person(int id, bool show, String firstName, String lastName, String gender, DateTime date_of_birth, String image_url, String occupation) {
    this.id = id;
    this.show = show;
    this.first_name = firstName;
    this.last_name = lastName;
    this.gender = gender;
    this.date_of_birth = date_of_birth;
    this.image_url = image_url;
    this.occupation = occupation;
    this.age = _getAge(date_of_birth);
  }

  static Person fromJson(Map<String, dynamic> json) {
    return Person(
      json['id'],
      json['show'],
      json['first_name'],
      json['last_name'],
      json['gender'],
      DateTime.parse(json['date_of_birth']),
      json['image_1'],
      json['occupation']
    );
  }

  int _getAge(DateTime date_of_birth) {
    return Age.dateDifference(fromDate: date_of_birth, toDate: DateTime.now()).years;
  }
}

class Like {
  int like_id;
  int person_id;
  DateTime dateTime;

  Like(int like_id, int person_id, DateTime dateTime) {
    this.like_id = like_id;
    this.person_id = person_id;
    this.dateTime = dateTime;
  }

  static Like fromJson(Map<String, dynamic> json) {
    return Like(
      json['like_id'],
      json['person_id'],
      DateTime.parse(json['datetime'])
    );
  }
}

class Likes {
  List<Like> likes;
  List<Like> liked_by;
  List<Like> matches;

  Likes(List<Like> likes, List<Like> liked_by, List<Like> matches) {
    this.likes = likes;
    this.liked_by = liked_by;
    this.matches = matches;
  }

  static List<Like> jsonToLikeList(List<dynamic> json) {
    return json.map((likeJson) {
      return Like.fromJson(likeJson);
    }).toList();
  }

  static Likes fromJson(Map<String, dynamic> json) {
    return Likes(
        jsonToLikeList(json['likes']),
        jsonToLikeList(json['liked_by']),
        jsonToLikeList(json['matches'])
    );
  }
}

class PersonFutures {

//  static var baseUrl = "http://192.168.178.17:8000";
  static var baseUrl = User.baseUrl;
  http.Client client;

  PersonFutures(http.Client client) {
    this.client = client;
  }

  Future<Likes> retrieveLikes(id) async {
    var url = baseUrl + '/persons/' + id.toString() + '/likes/';
    var httpResponse = await this.client.get(Uri.parse(url),
        headers: {HttpHeaders.authorizationHeader: "JWT "+ User.idTokenJWTString});
    if (httpResponse.statusCode == 200) {
      return Likes.fromJson(jsonDecode(httpResponse.body));
    }
    throw("Response statuscode is not 200");
  }

  Future<Person> retrievePerson(id) async {
    var url = baseUrl + '/persons/' + id.toString() + '/';
    var httpResponse = await this.client.get(Uri.parse(url),
        headers: {HttpHeaders.authorizationHeader: "JWT "+ User.idTokenJWTString});
    if (httpResponse.statusCode == 200) {
      return Person.fromJson(jsonDecode(httpResponse.body));
    }
    throw("Response statuscode is not 200");
  }

//  Future<Person> changeProfile(id) async {
//    var url = baseUrl + '/persons/' + id.toString() + '/';
//    var httpResponse = await this.client.put(url,
//      headers: {HttpHeaders.authorizationHeader: "JWT "+ User.idTokenJWTString},
//      body:
//    );
//  }

  Future<Person> loginPerson(String fcm_token) async {
    var url = baseUrl + '/persons/login/';
    var httpResponse = await this.client.post(
        Uri.parse(url),
        body: {'fcm_token': fcm_token},
        headers: {HttpHeaders.authorizationHeader: "JWT "+ User.idTokenJWTString}
    );
    if (httpResponse.statusCode == 200) {
      return Person.fromJson(jsonDecode(httpResponse.body));
    }
    throw("Response statuscode is not 200");
  }

  Future<Person> logoutPerson() async {
    var url = baseUrl + '/persons/logout/';
    var httpResponse = await this.client.get(
      Uri.parse(url),
      headers: {HttpHeaders.authorizationHeader: "JWT " + User.idTokenJWTString}
    );
    if (httpResponse.statusCode == 200) {
      return Person.fromJson(jsonDecode(httpResponse.body));
    }
    throw("Response statuscode is not 200");
  }

  Future<List<Person>> retrievePersons() async {
    var url = baseUrl + '/persons/';
    var httpResponse = await client.get(Uri.parse(url),
        headers: {HttpHeaders.authorizationHeader: "JWT "+ User.idTokenJWTString});
    if (httpResponse.statusCode == 200) {
      var json = jsonDecode(httpResponse.body);
      if (json is List) {
        return json.map((personMap) {
          return Person.fromJson(personMap);
        }).toList();
      }
      throw("Response is not in the right format.");
    }
    throw("Response statuscode is not 200");
  }
}
