import 'dart:io';

import 'package:date_app/drawer.dart';
import 'package:date_app/main.dart';
import 'package:date_app/person.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';

import 'user.dart';

Dio dio = new Dio();

class ProfileImage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    return _ProfileImageState();
  }

}

class _ProfileImageState extends State<ProfileImage> {

  Image _profileImage = null;
  File _imageFromGallery = null;
  BuildContext context;

  void initState() {
    if (User.ownPerson.image_url != null) {
      _profileImage = Image.network(User.ownPerson.image_url, height: 160,);
    } else {
      _profileImage = Image.asset("assets/profile.png", height: 160,);
    }
  }

  Future getProfileImageFromGallery() async {
    // _imageFromGallery = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _profileImage = Image.file(_imageFromGallery, height: 160);
    });

    var multipartFile = await MultipartFile.fromFile(_imageFromGallery.path);
    FormData formData = new FormData.fromMap({
      "image_1": multipartFile,
    });

    dio.patch(
        User.baseUrl + "/persons/" + User.ownId.toString() + "/",
        data: formData,
        options: Options(
            headers: {
              "authorization": "JWT " + User.idTokenJWTString,
            }
    )).catchError((error) {
        print(error.response);
    }).then((response) {
//        User.ownPerson = response.data;
        moveToSearchOnShow(context);
    });
  }

  @override
  Widget build(BuildContext context) {

    this.context = context;
    // TODO: implement build
    return Column(
      children: <Widget>[
        _profileImage,
        FlatButton(
          onPressed: getProfileImageFromGallery,
          child: Text("Upload Profile Image"),
        ),
      ],
    );
  }
}

class ChangeProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // final _changeProfileFormKey = GlobalKey<FormBuilderState>();


    // TODO: implement build
    return Scaffold(
      appBar: AppBar(title: Text("Change Profile")),
      drawer: DrawerWidget(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("abc")
//         child: FormBuilder(
//               key: _changeProfileFormKey,
//               autovalidate: true,
//               child: ListView(
//                 children: <Widget>[
//                   Padding(
//                     padding: const EdgeInsets.all(8.0),
//                     child: User.ownPerson != null && User.ownPerson.show == false ?
//                       Text("Please upload an image and create a profile to continue.",
//                         textAlign: TextAlign.center,
//                         style: TextStyle(
//                           color: Color.fromARGB(255, 38, 52, 93),
//                           fontWeight: FontWeight.w600,
//                           fontSize: 22,
//                         )
//                       )
//                     : Container(),
//                   ),
//                   ProfileImage(),
//                   FormBuilderTextField(
//                     attribute: "first_name",
//                     decoration: InputDecoration(labelText: "First Name"),
//                     initialValue: User.ownPerson != null ? User.ownPerson.first_name : "",
//                     validators: [FormBuilderValidators.required(), FormBuilderValidators.maxLength(80)],
//                   ),
//                   FormBuilderDateTimePicker(
//                       attribute: "date_of_birth",
//                       inputType: InputType.date,
//                       initialValue: User.ownPerson != null ? User.ownPerson.date_of_birth : "",
//                       format: DateFormat("yyyy-MM-dd"),
//                       decoration: InputDecoration(labelText: "Date of Birth"),
//                       validators: [FormBuilderValidators.required()],
//                       valueTransformer: (dateTime) {
//                         return DateFormat("yyyy-MM-dd").format(dateTime);
//                       }
//                   ),
//                   FormBuilderDropdown(
//                     attribute: "gender",
//                     decoration: InputDecoration(labelText: "Gender"),
//                     initialValue: User.ownPerson != null ? User.ownPerson.gender : "",
//                     items: ['Male','Female'].map((gender) => DropdownMenuItem(
//                       value: gender.compareTo('Male') == 0 ? "M" : "F",
//                       child: Text(gender),
//                     )).toList(),
//                     validators: [FormBuilderValidators.required()],
//                   ),
//                   FormBuilderTextField(
//                     attribute: "occupation",
//                     decoration: InputDecoration(labelText: "Occupation",),
//                     initialValue: User.ownPerson != null ? User.ownPerson.occupation : "",
//                     validators: [FormBuilderValidators.required(), FormBuilderValidators.maxLength(120)],
//                   ),
// //                  FormBuilderTextField(
// //                    attribute: "profile",
// //                    decoration: InputDecoration(labelText: "Profile Text",),
// ////                    initialValue: User.ownPerson != null ? User.ownPerson.pr : "",
// //                    validators: [FormBuilderValidators.maxLength(300)],
// //                  ),
//                   FlatButton(
//                     child: Text("Update Profile"),
//                     onPressed: () {
//                       if (_changeProfileFormKey.currentState.saveAndValidate()) {
//                         dio.patch(
//                             User.baseUrl + "/persons/" + User.ownId.toString() + "/",
//                             data: new FormData.fromMap(
//                                 _changeProfileFormKey.currentState.value
//                             ),
//                             options: Options(
//                                 headers: {
//                                   "authorization": "JWT " + User.idTokenJWTString,
//                                 }
//                         )).catchError((error) {
//                           print(error.response);
//                         }).then((response) {
//                           User.ownPerson = Person.fromJson(response.data);
//                           moveToSearchOnShow(context);
//                         });
//                       }
//                     },
//                   )
//                 ],
//               ),
//             )
          )
      );
  }
}

void moveToSearchOnShow(BuildContext context) {
  if (User.ownPerson.show) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => MyHomePage(title: "Search")),
    );
  }
}
