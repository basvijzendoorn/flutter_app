import 'package:date_app/authentication.dart';
import 'package:date_app/drawer.dart';
import 'package:date_app/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'changeProfile.dart';
import 'firebaseCloudMessaging.dart';
import 'main.dart';

class LoginPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
      ),
//      drawer: DrawerWidget(),
      body: Container(
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
//              FlutterLogo(size: 150),
              SizedBox(height: 50),
              _signInButton(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _signInButton(BuildContext context) {
    return OutlineButton(
        splashColor: Colors.grey,
        onPressed: () {
          signInWithGoogle().then((successMessage) {
            FirebaseCloudMessaging.getFCMToken().then((fcm_token) {
              personFutures.loginPerson(fcm_token).then((person) {
                User.ownId = person.id;
                User.ownPerson = person;
                print("succesfully logged in with person id " + User.ownId.toString() + " and fcm_token " + person.toString());
                if (User.ownPerson.show){
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => MyHomePage(title: "Search"))
                  );
                } else {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => ChangeProfile())
                  );
                }
              });
            });
          });
        },
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
        highlightElevation: 0,
        borderSide: BorderSide(color: Colors.grey),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image(image: AssetImage("assets/google_logo.png"), height: 35.0),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  'Sign in with Google',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.grey,
                  ),
                ),
              )
            ],
          )
        )
    );
  }
}