import 'dart:convert';
import 'dart:io';

import 'package:date_app/drawer.dart';
import 'package:date_app/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'firebaseCloudMessaging.dart';
import "package:http/http.dart" as http;

MessageFutures messageFutures = new MessageFutures(http.Client());

class Chatting extends StatelessWidget {
  const Chatting({
    Key key,
    this.person_id,
  }) : super(key: key);

  final int person_id;

  getChattingBox(Message message) {
    return Container(
      alignment: message.fromOther() ? AlignmentDirectional.centerStart : AlignmentDirectional.centerEnd,
      child:
        Container(
          margin: message.fromOther()
              ? EdgeInsets.only(
            top: 4.0,
            bottom: 4.0,
            right: 40.0
          )
              : EdgeInsets.only(
            top: 4.0,
            bottom: 4.0,
            left: 40.0,
          ),
          padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
//          alignment: fromMe() ? Alignment.centerLeft : Alignment.centerRight,
//          color: fromMe() ? Colors.black12 : Color.fromARGB(255, 38, 52, 93),
          decoration: BoxDecoration(
            color: message.fromOther() ? Colors.black12 : Colors.blue,
            borderRadius: message.fromOther()
                ? BorderRadius.only(
              topRight: Radius.circular(16.0),
              bottomRight: Radius.circular(16.0),
            )
            : BorderRadius.only(
              topLeft: Radius.circular(16.0),
              bottomLeft: Radius.circular(16.0),
            ),
          ),
          child: Text(
              message.message,
              style: TextStyle(
                color: message.fromOther() ? Colors.black : Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.w600,
              )
          ),
        )
      ,
    );
  }

  @override
  Widget build(BuildContext context) {
    var messageList = [];

    var chatFutureBuilder = FutureBuilder(
      future: messageFutures.getChatMessages(this.person_id),
      builder: (BuildContext context, AsyncSnapshot<List<Message>> snapshot) {
        if (snapshot.hasData) {
          messageList = snapshot.data;
        }

        return StreamBuilder(
            stream: FirebaseCloudMessaging.messageStream,
            builder: (BuildContext context, AsyncSnapshot<Message> snapshot) {
              if (snapshot.hasData) {
                var message = snapshot.data;
                if (message.from_person == this.person_id ||
                    message.to_person == this.person_id) {
                  messageList.add(message);
                }
              }
              messageList.sort((message_1, message_2) =>
                  message_2.dateTime.compareTo(message_1.dateTime));
              return ListView.builder(
                itemCount: messageList.length,
                itemBuilder: (BuildContext context, int index) {
                  return getChattingBox(messageList[index]);
                },
                reverse: true,
              );
            }
        );
      },
    );


//    var messages = [Message(1, Message.personIdToUrl(1), Message.personIdToUrl(2), DateTime.now(), "message"), Message(2, Message.personIdToUrl(2), Message.personIdToUrl(1), DateTime.now(), "another message")];

    var textEditingController = new TextEditingController();

    return Scaffold(
      appBar: AppBar(
        title: Text("Chatting"),
      ),
      body: Column(
          children: [
            Expanded(
              child: chatFutureBuilder
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 8.0),
              height: 70.0,
              color: Colors.white,
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      textCapitalization: TextCapitalization.sentences,
                      onChanged: (value) {},
                      controller: textEditingController,
//                      onSubmitted:
                      decoration: InputDecoration.collapsed(
                        hintText: 'Send a message...',
                      ),
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.send),
                    iconSize: 25.0,
                    color: Theme.of(context).primaryColor,
                    onPressed: () {
                      var message = Message(
                          0,
                          User.ownId,
                          this.person_id,
                          DateTime.now(),
                          textEditingController.value.text,
                      );
                      textEditingController.clear();
                      var sendMessage = messageFutures.sendMessage(message);
                      sendMessage.then((message) {
                        FirebaseCloudMessaging.messageStreamController.add(message);
                      });
                    },
                  ),
                ],
              ),
            )
          ]
      ),
      drawer: DrawerWidget(),
    );
  }
}

class MessageFutures {
  static var baseUrl = User.baseUrl;
  http.Client client;

  MessageFutures(http.Client client) {
    this.client = client;
  }

  Future<Message> sendMessage(Message message) async {
    var url = baseUrl + '/messages/';
    var jsonMessage = message.toJson();
    print("json_message: " + jsonMessage.toString());
    var httpResponse = await this.client.post(Uri.parse(url),
        body: jsonMessage,
        headers: {HttpHeaders.authorizationHeader: "JWT "+ User.idTokenJWTString}
    );
    if (httpResponse.statusCode == 201) {
      return Message.fromJson(jsonDecode(httpResponse.body));
    }
    throw("response body: " + httpResponse.body);
  }

  Future<List<Message>> getChatMessages(int other_person) async {
    var url = baseUrl + '/messages/chat/';
    var httpResponse = await this.client.post(Uri.parse(url),
      body: {
        'other_person' : other_person.toString(),
      },
      headers: {HttpHeaders.authorizationHeader: "JWT "+ User.idTokenJWTString}
    );
    if (httpResponse.statusCode == 200) {
      var json = jsonDecode(httpResponse.body);
      if (json is List) {
        return json.map((messageMap) {
          return Message.fromJson(messageMap);
        }).toList();
      }
      throw("Response is not in the right format");
    }
    throw("Response statuscode is not 200");
  }
}

class Message {
  int id;
  int from_person;
  int to_person;
  DateTime dateTime;
  String message;

  Message(int id, int from_person, int to_person, DateTime dateTime, String message) {
    this.id = id;
    this.from_person = from_person;
    this.to_person = to_person;
    this.dateTime = dateTime;
    this.message = message;
  }

  static Message fromJson(var json) {
    return Message(
      json['id'],
      json['from_person'],
      json['to_person'],
      DateTime.parse(json['date']),
      json['message']
    );
  }

  Map<String, dynamic> toJson() {
    return {
//      'id':  this.id,
      'from_person': this.from_person.toString(),
      'to_person': this.to_person.toString(),
      'date': this.dateTime.toString(),
      'message': this.message
    };
  }

  bool fromOther() {
    return from_person != User.ownId;
  }

  static int personIdToUrl(int person_id) {
//    return User.baseUrl + "/persons/" + person_id.toString() + "/";
    return person_id;
  }

}
