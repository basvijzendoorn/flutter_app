import 'package:date_app/authentication.dart';
import 'package:date_app/changeProfile.dart';
import 'package:date_app/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'login.dart';
import 'main.dart';

class DrawerWidget extends StatelessWidget {
  const DrawerWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var menuItems = <Widget> [];

    if (User.loggedIn && User.ownPerson.show) {
      menuItems.add(
          ListTile(
            title: Text("Search"),
            onTap: () {
//            Navigator.of(context).pop();
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => MyHomePage(title: "Search")),
              );
            },
          )
      );
    }
    if (User.loggedIn) {
      menuItems.add(ListTile(
        title: Text("Change Profile"),
        onTap: () {
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => ChangeProfile()));
        },
      ));
      menuItems.add(ListTile(title: Text("Logout"),
        onTap: () {
          signOutGoogle();
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => LoginPage())
          );
        }
      ));
    }
//          ListTile(
//            title: Text("Likes"),
//            onTap: () {
//              Navigator.push(
//                  context,
//                  MaterialPageRoute(builder: (context) => LikesPage())
//              );
//            },
//          ),
//          ListTile(
//            title: Text("Chat"),
//            onTap: () {
//              Navigator.push(
//                  context,
//                  MaterialPageRoute(builder: (context) => ChattingPage())
//              );
//            },
//          ),

//          ListTile(
//            title: Text("Profile"),
//            onTap: () {
//              Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage()));
//            },
//          ),
    else {
      menuItems = [ListTile(
        title: Text("Login"),
        onTap: () {
//          Navigator.of(context).pop();
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => LoginPage())
          );
        },
      )];
    }

    return Drawer (
      child: ListView(
        children: menuItems
      ),
    );
  }
}
