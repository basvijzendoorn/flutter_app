import 'package:date_app/drawer.dart';
import 'package:date_app/main.dart';
import 'package:date_app/person.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var profileFutureBuilder = FutureBuilder(
      future: personFutures.retrievePerson(1),
      builder: (BuildContext context, AsyncSnapshot<Person> snapshot) {
          if (snapshot.hasData) {
            return ListView(
              children: <Widget>[
                  Image.network(snapshot.data.image_url),
                  Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                          snapshot.data.first_name + " (" + snapshot.data.age.toString() + ") ",
                          style: TextStyle(
                            fontSize: 32,
                          ),
                      ),
                      color: Colors.white,
                      padding: EdgeInsets.all(10),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      snapshot.data.occupation,
                      style: TextStyle(
                        fontSize: 22,
                      ),
                    ),
                    color: Colors.white,
                    padding: EdgeInsets.fromLTRB(10, 0, 0, 10),
                  ),
//                  Container(
//                    alignment: Alignment.centerLeft,
//                    child: Text(
//                      "Dit is een profile text",
//                      style: TextStyle(
//                        fontSize: 16,
//                      ),
//                    ),
//                    color: Colors.white,
//                    padding: EdgeInsets.fromLTRB(10, 0, 0, 10),
//                  ),
              ],
            );

//            return Container(
//              decoration: BoxDecoration(
//                image: DecorationImage(
//                    image: NetworkImage(snapshot.data.image_url),
//                    fit: BoxFit.cover
//                ),
//              ),
//              child: Container(
//                  alignment: Alignment.bottomCenter,
//                  child: Text("Bas"),
//                  color: Colors.white,
//                  padding: EdgeInsets.all(30),
//              ),
//            );
          }
          return Container();
      }
    );

    return Scaffold(
      appBar: AppBar(title: Text("Profile Page")),
      drawer: DrawerWidget(),
      body: profileFutureBuilder,
    );
  }

}