import 'package:date_app/chattingPage.dart';
import 'package:date_app/firebaseCloudMessaging.dart';
import 'package:date_app/login.dart';
import 'package:date_app/person.dart';
import 'package:date_app/user.dart';
import 'package:date_app/video.dart';
import 'package:flutter/material.dart';
import "package:http/http.dart" as http;

import 'drawer.dart';

void main() {
  runApp(MyApp());
  FirebaseCloudMessaging.init();
}
PersonFutures personFutures = new PersonFutures(http.Client());


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(),
    );
  }
}

class LikesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var listViewFutureBuilder = FutureBuilder(
      future: personFutures.retrieveLikes(User.ownId),
      builder: (BuildContext context, AsyncSnapshot<Likes> snapshot) {
        var listElements = <Widget>[];

        if (snapshot.hasData) {
          snapshot.data.likes.forEach((like) {
            listElements.add(PersonRow(like));
          });
        }

        return ListView(children: listElements);
      },
    );

    return Scaffold(
      appBar: AppBar(title: Text("Likes Page")),
      body: listViewFutureBuilder,
      drawer: DrawerWidget(),
    );
  }
}

class PersonRow extends StatelessWidget {
  Person person;
  Like like;

  PersonRow(Like like) {
    this.like = like;
  }

  @override
  Widget build(BuildContext context) {
    var rowFutureBuilder = FutureBuilder(
      future: personFutures.retrievePerson(like.person_id),
      builder: (BuildContext context, AsyncSnapshot<Person> snapshot) {
        var name = "";
        if (snapshot.hasData) {
          name = snapshot.data.first_name + " " + snapshot.data.last_name;
        }

        return Row(
          children: <Widget>[
            Expanded(
                flex: 4,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.asset("assets/round_circle.png"),
                )),
            Expanded(
                flex: 10,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(name,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: Color.fromARGB(255, 18, 25, 43),
                        fontFamily: "",
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        height: 1.5,
                      )),
                )),
            Expanded(
                flex: 5,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlatButton(
                    onPressed: () => {},
                    color: Color.fromARGB(255, 74, 144, 226),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                    ),
                    padding: EdgeInsets.all(0),
                    child: Text(
                      "Like",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Color.fromARGB(255, 255, 255, 255),
                        fontFamily: "Helvetica",
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                      ),
                    ),
                  ),
                ))
          ],
        );
      },
    );

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: rowFutureBuilder,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    var gridViewFutureBuilder = FutureBuilder(
      future: personFutures.retrievePersons(),
      builder: (BuildContext context, AsyncSnapshot<List<Person>> snapshot) {
        var gridElements = <Widget>[];

        if (snapshot.hasData) {
          snapshot.data.forEach((person) {
            if (person.id != User.ownId && person.show) {
              gridElements.add(GridElement(person));
            }
          });
        }

        return GridView.count(
            crossAxisCount: 2,
            childAspectRatio: 1.0 / 1.3,
            children: gridElements);
      },
    );

    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: gridViewFutureBuilder,
      drawer: DrawerWidget(),
    );
  }
}

class GridElement extends StatelessWidget {
  Person person;

  GridElement(Person person) {
    this.person = person;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Flexible(
          flex: 8,
          child: Align(
            alignment: Alignment.topCenter,
//            child: Stack(children: <Widget>[getPersonImage(context), getHeartImage()]),
              child: Stack(children: <Widget>[getPersonImage(context)]),
          ),
        ),
        Flexible(
          flex: 1,
          child: getNameAndAgeText(),
        ),
        Flexible(
          flex: 1,
          child: getOccupationText(),
        )
      ],
    );
  }

  Padding getHeartImage() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        alignment: Alignment.bottomRight,
        child: FractionallySizedBox(
          child: Image.asset('assets/color-blue.png'),
          widthFactor: 0.36,
          heightFactor: 0.36,
        ),
      ),
    );
  }

  GestureDetector getPersonImage(BuildContext context) {
    return GestureDetector(
      child: Image.network(person.image_url, fit: BoxFit.fitWidth),
      onTap:() {
        print("Image clicked" + person.image_url);
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Chatting(
                person_id: person.id,
            ))
        );
      }
    );
  }

  Text getOccupationText() {
    return Text(
      person.occupation,
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Color.fromARGB(255, 38, 52, 93),
        fontWeight: FontWeight.w400,
        fontSize: 12,
      ),
    );
  }

  Text getNameAndAgeText() {
    return Text(
      person.first_name + ", " + person.age.toString(),
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Color.fromARGB(255, 38, 52, 93),
        fontWeight: FontWeight.w400,
        fontSize: 16,
      ),
    );
  }
}
