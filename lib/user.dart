import 'package:date_app/person.dart';

class User {
  static int ownId = 0;
  static Person ownPerson = null;
  static String idTokenJWTString;
  static bool loggedIn = false;
  static var serverUrl = "http://www.feeldateapp.com";
  static var baseUrl = serverUrl + "/app";
}