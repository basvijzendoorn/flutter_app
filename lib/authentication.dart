import 'package:date_app/main.dart';
import 'package:date_app/person.dart';
import 'package:date_app/user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import "package:http/http.dart" as http;


final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn googleSignIn = GoogleSignIn();

Future<String> signInWithGoogle() async {
  final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
  final GoogleSignInAuthentication googleSignInAuthentication =
      await googleSignInAccount.authentication;

  final AuthCredential credential = GoogleAuthProvider.getCredential(
    accessToken: googleSignInAuthentication.accessToken,
    idToken: googleSignInAuthentication.idToken,
  );

  final AuthResult authResult = await _auth.signInWithCredential(credential);
  final FirebaseUser user = authResult.user;

  assert(!user.isAnonymous);
  assert(await user.getIdToken() != null);
  var idTokenResult = await user.getIdToken();
  User.idTokenJWTString = idTokenResult.token;

  PersonFutures personFutures = new PersonFutures(http.Client());

  final FirebaseUser currentUser = await _auth.currentUser();
  assert(user.uid == currentUser.uid);

  User.loggedIn = true;
  return 'signInWithGoogle succeeded: $user';
}

void signOutGoogle() async {
  await googleSignIn.signOut();

  personFutures.logoutPerson().then((person) {
    User.ownId = 0;
    print("succesfully logged out with fcm_token" + person.toString());
  });

  User.loggedIn = false;
  print("User Sign Out");
}
