import 'dart:async';
import 'dart:convert';

import 'package:date_app/chattingPage.dart';
import 'package:date_app/user.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class FirebaseCloudMessaging {


  static final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

//  static final StreamController<Map<String, dynamic>> _onMessageStreamController = new StreamController.broadcast();
  static final StreamController<Map<String, dynamic>> _genericStreamController = new StreamController.broadcast();
  static final Stream<Map<String, dynamic>> genericStream = _genericStreamController.stream;

  static final StreamController<Message> messageStreamController = new StreamController.broadcast();
  static final Stream<Message> messageStream = messageStreamController.stream;

  static init() {
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> firebaseMessage) async {
          var fcmMessage = FcmMessage.fromJson(firebaseMessage);
          if (fcmMessage.message.from_person == User.ownId || fcmMessage.message.to_person == User.ownId) {
            messageStreamController.add(fcmMessage.message);
          }
          print('on message $firebaseMessage');
        }, onLaunch: (Map<String, dynamic> message) async {
          _genericStreamController.add(message);
             print('on launch $message');
        }, onResume: (Map<String, dynamic> firebaseMessage) async {
          var fcmMessage = FcmMessage.fromJson(firebaseMessage);
          if (fcmMessage.message.from_person == User.ownId || fcmMessage.message.to_person == User.ownId) {
            messageStreamController.add(fcmMessage.message);
          }
          print('on resume $firebaseMessage');
    }, );
    _firebaseMessaging.getToken().then((token) => print("Firebase token: " + token));
  }

  static Future<String> getFCMToken() {
    return _firebaseMessaging.getToken();
  }
}

class FcmMessage {
  FcmNotification fcmNotification;
  Message message;

  FcmMessage(FcmNotification fcmNotification, Message message) {
    this.fcmNotification = fcmNotification;
    this.message = message;
  }

  static FcmMessage fromJson(var json) {
    var messageJson = json['data'];
    return FcmMessage(
      FcmNotification.fromJson(json['notification']),
      new Message(int.parse(messageJson['id']),
        int.parse(messageJson['from_person']),
        int.parse(messageJson['to_person']),
        DateTime.parse(messageJson['date']),
        messageJson['message']
      )
    );
  }
}

class FcmNotification {
  var body;
  String title;

  FcmNotification(var body, String title) {
    this.body = body;
    this.title = title;
  }

  static FcmNotification fromJson(var json) {
      return FcmNotification(
        json['body'],
        json['title']
      );
  }
}

